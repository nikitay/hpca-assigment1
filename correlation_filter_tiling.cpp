#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>
#include <fstream>
using namespace std;

#define FILTER_SIZE 3
#define FILTERS 2
#define TILEROW 32
#define TILECOL 32

int min(int a, int b)
{
    if (a < b)
        return a;
    return b;
}

// Optimize this function - row major order and split filter and loop unrolling and tiling
void applyFilters(int *image, int height, int width, int *filter, int filters)
{
    int *copy = new int[height * width];
    int filter_size = FILTER_SIZE * FILTER_SIZE;
    int *_filter1 = new int[filter_size];
    int *_filter2 = new int[filter_size];

    // partitioned to reduce number of calculations
    for(int k = 0; k < filter_size; k++)
    {
        _filter1[k] = filter[k];
        _filter2[k] = filter[filter_size + k];
    }

    for(int r = 0; r < height; r++)
    {
        int endr = min(r + TILEROW, height);
        for(int c = 0; c < width; c++)
        {
            int endc = min(c + TILECOL, width);
            for(int i = r; i < endr ; i++)
            {
                int base = i*width;
                for(int j = c; j < endc; j++)
                {
                    copy[base + j] = 0;
                    if(i - 1 >= 0 && i + 1 < height && j - 1 >= 0 and j + 1 < width) 
                    {
                        copy[base + j] += image[(i-1) * width + -1 + j] * _filter1[(1 + -1) * FILTER_SIZE + 1 + -1];
                        copy[base + j] += image[(-1 + i) * width + 0 + j] * _filter1[(1 + -1) * FILTER_SIZE + 1 + 0];
                        copy[base + j] += image[(-1 + i) * width + 1 + j] * _filter1[(1 + -1) * FILTER_SIZE + 1 + 1];
                        copy[base + j] += image[(0 + i) * width + -1 + j] * _filter1[(1 + 0) * FILTER_SIZE + 1 + -1];
                        copy[base + j] += image[(0 + i) * width + 0 + j] * _filter1[(1 + 0) * FILTER_SIZE + 1 + 0];
                        copy[base + j] += image[(0 + i) * width + 1 + j] * _filter1[(1 + 0) * FILTER_SIZE + 1 + 1];
                        copy[base + j] += image[(1 + i) * width + -1 + j] * _filter1[(1 + 1) * FILTER_SIZE + 1 + -1];
                        copy[base + j] += image[(1 + i) * width + 0 + j] * _filter1[(1 + 1) * FILTER_SIZE + 1 + 0];
                        copy[base + j] += image[(1 + i) * width + 1 + j] * _filter1[(1 + 1) * FILTER_SIZE + 1 + 1];
                        
                    }
                    else
                    {
                        for(int offx = -1; offx <= 1; ++offx)
                        {
                            for(int offy = -1; offy <= 1; ++offy)
                            {
                                if(offx + i < 0 || offx + i >= height || offy + j < 0 || offy + j >= width)
                                    continue;
                                copy[base + j] += image[(offx + i) * width + offy + j] * 
                                    _filter1[(1 + offx) * FILTER_SIZE + 1 + offy];
                            }
                        }
                    }
                }
            }
            int ptrrd, ptrcd;
            if (endr == height)
            {
                ptrrd = height;
            }
            else {
                ptrrd =  endr - 1;
            }
            if (endc == width)
            {
                ptrcd = width;
            }
            else {
                ptrcd =  endc - 1;
            }
            int ptrrs, ptrcs;
            if (r == 0) {
                ptrrs = 0;
            }
            else {
                ptrrs = r-1;
            }
            if (c == 0) {
                ptrcs = 0;
            }
            else {
                ptrcs = c-1;
            }
            for(int i = ptrrs; i < ptrrd ; i++)
            {
                int base = i*width;
                for(int j = ptrcs; j < ptrcd; j++)
                {
                    image[base + j] = 0;
                    if(i - 1 >= 0 && i + 1 < height && j - 1 >= 0 and j + 1 < width) 
                    {
                        image[base + j] += copy[(i-1) * width + -1 + j] * _filter2[(1 + -1) * FILTER_SIZE + 1 + -1];
                        image[base + j] += copy[(-1 + i) * width + 0 + j] * _filter2[(1 + -1) * FILTER_SIZE + 1 + 0];
                        image[base + j] += copy[(-1 + i) * width + 1 + j] * _filter2[(1 + -1) * FILTER_SIZE + 1 + 1];
                        image[base + j] += copy[(0 + i) * width + -1 + j] * _filter2[(1 + 0) * FILTER_SIZE + 1 + -1];
                        image[base + j] += copy[(0 + i) * width + 0 + j] * _filter2[(1 + 0) * FILTER_SIZE + 1 + 0];
                        image[base + j] += copy[(0 + i) * width + 1 + j] * _filter2[(1 + 0) * FILTER_SIZE + 1 + 1];
                        image[base + j] += copy[(1 + i) * width + -1 + j] * _filter2[(1 + 1) * FILTER_SIZE + 1 + -1];
                        image[base + j] += copy[(1 + i) * width + 0 + j] * _filter2[(1 + 1) * FILTER_SIZE + 1 + 0];
                        image[base + j] += copy[(1 + i) * width + 1 + j] * _filter2[(1 + 1) * FILTER_SIZE + 1 + 1];
                        
                    }
                    else
                    {
                        for(int offx = -1; offx <= 1; ++offx)
                        {
                            for(int offy = -1; offy <= 1; ++offy)
                            {
                                if(offx + i < 0 || offx + i >= height || offy + j < 0 || offy + j >= width)
                                    continue;
                                image[base + j] += copy[(offx + i) * width + offy + j] * 
                                    _filter2[(1 + offx) * FILTER_SIZE + 1 + offy];
                            }
                        }
                    }
                }
            }
            c = endc - 1;
        }
        r = endr - 1;
    }
    delete copy;
}

int main()
{
    int ht, wd;
    cin >> ht >> wd;
    int *img = new int[ht * wd];
    for(int i = 0; i < ht; ++i)
        for(int j = 0; j < wd; ++j)
            cin >> img[i * wd + j];
            
    int filters = FILTERS;
    int *filter = new int[filters * FILTER_SIZE * FILTER_SIZE];
    for(int i = 0; i < filters; ++i)
        for(int j = 0; j < FILTER_SIZE; ++j)
            for(int k = 0; k < FILTER_SIZE; ++k)
                cin >> filter[i * FILTER_SIZE * FILTER_SIZE + j * FILTER_SIZE + k];
    
    clock_t begin = clock();
    applyFilters(img, ht, wd, filter, filters);
    clock_t end = clock();
    cout << "Execution time: " << double(end - begin) / (double)CLOCKS_PER_SEC << " seconds\n";
    ofstream fout("outputtiling.txt");
    for(int i = 0; i < ht; ++i)
    {
        for(int j = 0; j < wd; ++j)
            fout << img[i * wd + j] << " ";
        fout << "\n";
    }
}
