#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>
#include <fstream>
using namespace std;

#define FILTER_SIZE 3
#define FILTERS 2

// Optimize this function -  row major order and split filter and loop unrolling
void applyFilters(int *image, int height, int width, int *filter, int filters)
{
    for(int i = 0; i < filters; ++i)
    {
        int *copy = new int[height * width];
        int filter_size = FILTER_SIZE * FILTER_SIZE;
        int *_filter = new int[filter_size];

        // partitioned to reduce number of calculations
        for(int k = 0; k < filter_size; k++)
        {
            _filter[k] = filter[i * filter_size + k];
        }
        
        for(int r = 0; r < height; r++)
        {
            int base = r*width;
            for(int c = 0; c < width; ++c)
            {
                copy[base + c] = 0;
                if(r - 1 >= 0 && r + 1 < height && c - 1 >= 0 and c + 1 < width) 
                {
                    copy[base + c] += image[(r-1) * width + -1 + c] * _filter[(1 + -1) * FILTER_SIZE + 1 + -1];
                    copy[base + c] += image[(-1 + r) * width + 0 + c] * _filter[(1 + -1) * FILTER_SIZE + 1 + 0];
                    copy[base + c] += image[(-1 + r) * width + 1 + c] * _filter[(1 + -1) * FILTER_SIZE + 1 + 1];
                    copy[base + c] += image[(0 + r) * width + -1 + c] * _filter[(1 + 0) * FILTER_SIZE + 1 + -1];
                    copy[base + c] += image[(0 + r) * width + 0 + c] * _filter[(1 + 0) * FILTER_SIZE + 1 + 0];
                    copy[base + c] += image[(0 + r) * width + 1 + c] * _filter[(1 + 0) * FILTER_SIZE + 1 + 1];
                    copy[base + c] += image[(1 + r) * width + -1 + c] * _filter[(1 + 1) * FILTER_SIZE + 1 + -1];
                    copy[base + c] += image[(1 + r) * width + 0 + c] * _filter[(1 + 1) * FILTER_SIZE + 1 + 0];
                    copy[base + c] += image[(1 + r) * width + 1 + c] * _filter[(1 + 1) * FILTER_SIZE + 1 + 1];
                    
                }
                else
                {
                    for(int offx = -1; offx <= 1; ++offx)
                    {
                        for(int offy = -1; offy <= 1; ++offy)
                        {
                            if(offx + r < 0 || offx + r >= height || offy + c < 0 || offy + c >= width)
                                continue;
                            copy[base + c] += image[(offx + r) * width + offy + c] * 
                                _filter[(1 + offx) * FILTER_SIZE + 1 + offy];
                        }
                    }
                }
            }
        }
        
	    for(int r = 0; r < height; ++r)
        {
            int base = r*width;
            for(int c = 0; c < width; ++c)
                image[base+c] = copy[base+c];
        }
        delete copy;
        delete _filter;
    }
}

int main()
{
    int ht, wd;
    cin >> ht >> wd;
    int *img = new int[ht * wd];
    for(int i = 0; i < ht; ++i)
        for(int j = 0; j < wd; ++j)
            cin >> img[i * wd + j];
            
    int filters = FILTERS;
    int *filter = new int[filters * FILTER_SIZE * FILTER_SIZE];
    for(int i = 0; i < filters; ++i)
        for(int j = 0; j < FILTER_SIZE; ++j)
            for(int k = 0; k < FILTER_SIZE; ++k)
                cin >> filter[i * FILTER_SIZE * FILTER_SIZE + j * FILTER_SIZE + k];
    
    clock_t begin = clock();
    applyFilters(img, ht, wd, filter, filters);
    clock_t end = clock();
    cout << "Execution time: " << double(end - begin) / (double)CLOCKS_PER_SEC << " seconds\n";
    ofstream fout("outputloop.txt");
    for(int i = 0; i < ht; ++i)
    {
        for(int j = 0; j < wd; ++j)
            fout << img[i * wd + j] << " ";
        fout << "\n";
    }
}
