#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>
#include <fstream>
using namespace std;

#define FILTER_SIZE 3
#define FILTERS 2

// Optimize this function -  row major order
void applyFilters(int *image, int height, int width, int *filter, int filters)
{
    for(int i = 0; i < filters; ++i)
    {
        int *copy = new int[height * width];
        
        for(int r = 0; r < height; ++r)
        {
            int base = r*width;
            for(int c = 0; c < width; ++c)
            {
                copy[base + c] = 0;
                for(int offx = -1; offx <= 1; ++offx)
                {
                    for(int offy = -1; offy <= 1; ++offy)
                    {
                        if(offx + r < 0 || offx + r >= height || offy + c < 0 || offy + c >= width)
                            continue;
                        copy[base + c] += image[(offx + r) * width + offy + c] * 
                            filter[i * FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
                    }
                }
            }
        }
        
	    for(int r = 0; r < height; ++r)
        {
            int base = r*width;
            for(int c = 0; c < width; ++c)
                image[base+c] = copy[base+c];
        }
        delete copy;
    }
}

int main()
{
    int ht, wd;
    cin >> ht >> wd;
    int *img = new int[ht * wd];
    for(int i = 0; i < ht; ++i)
        for(int j = 0; j < wd; ++j)
            cin >> img[i * wd + j];
            
    int filters = FILTERS;
    int *filter = new int[filters * FILTER_SIZE * FILTER_SIZE];
    for(int i = 0; i < filters; ++i)
        for(int j = 0; j < FILTER_SIZE; ++j)
            for(int k = 0; k < FILTER_SIZE; ++k)
                cin >> filter[i * FILTER_SIZE * FILTER_SIZE + j * FILTER_SIZE + k];
    
    clock_t begin = clock();
    applyFilters(img, ht, wd, filter, filters);
    clock_t end = clock();
    cout << "Execution time: " << double(end - begin) / (double)CLOCKS_PER_SEC << " seconds\n";
    ofstream fout("zrowmajor.txt");
    for(int i = 0; i < ht; ++i)
    {
        for(int j = 0; j < wd; ++j)
            fout << img[i * wd + j] << " ";
        fout << "\n";
    }
}
